package wxpay

import (
	"bytes"
	"crypto/md5"
	"crypto/tls"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
	"sort"
	"strconv"
	"strings"
	"time"
)

const (
	kUnifyOrderUrl = "https://api.mch.weixin.qq.com/pay/unifiedorder"
)

type WxPay struct {
	appId     string
	apiSecret string
	mchId     string
	tradeType string
	notifyUrl string
}

func MarshalXml(table map[string]string) ([]byte, error) {
	start := xml.StartElement{
		Name: xml.Name{Space: "", Local: "map"},
		Attr: []xml.Attr{},
	}

	tokens := []xml.Token{start}

	for key, value := range table {
		token := xml.StartElement{Name: xml.Name{"", key}}
		tokens = append(tokens, token, xml.CharData(value), xml.EndElement{token.Name})
	}

	tokens = append(tokens, xml.EndElement{start.Name})

	buf := bytes.NewBufferString("")
	encoder := xml.NewEncoder(buf)

	for _, token := range tokens {
		err := encoder.EncodeToken(token)
		if err != nil {
			return nil, err
		}
	}

	err := encoder.Flush()
	if err != nil {
		return nil, err
	}

	return buf.Bytes(), nil
}

func UnmarshalXml(buf []byte) map[string]string {
	table := make(map[string]string)
	values := make([]string, 0)

	decoder := xml.NewDecoder(bytes.NewBuffer(buf))

	for token, err := decoder.Token(); err == nil; token, err = decoder.Token() {
		switch t := token.(type) {
		case xml.CharData:
			values = append(values, string([]byte(t)))
		case xml.EndElement:
			if t.Name.Local != "xml" {
				table[t.Name.Local] = values[len(values)-1]
				values = values[:len(values)]
			}
		}
	}

	return table
}

func NewNonceString() string {
	nonce := strconv.FormatInt(time.Now().UnixNano(), 36)
	return fmt.Sprintf("%x", md5.Sum([]byte(nonce)))
}

func Md5Signature(table map[string]string, key string) string {
	var keys []string

	for k, v := range table {
		if k != "sign" && v != "" {
			keys = append(keys, k)
		}
	}

	sort.Strings(keys)

	var sortedParam []string
	for _, k := range keys {
		sortedParam = append(sortedParam, k+"="+table[k])
	}

	buf := strings.Join(sortedParam, "&") + "&key=" + key
	return fmt.Sprintf("%X", md5.Sum([]byte(buf)))
}

func DoHttpRequest(method, url string, body []byte) ([]byte, error) {
	req, err := http.NewRequest(method, url, bytes.NewBuffer(body))
	if err != nil {
		return nil, err
	}

	req.Header.Add("Content-type", "application/x-www-form-urlencoded;charset=UTF-8")

	trans := &http.Transport{TLSClientConfig: &tls.Config{InsecureSkipVerify: false}}
	client := &http.Client{Transport: trans}

	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()

	respData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return respData, nil
}

func NewWxPay(appId, apiSecret, mchId, tradeType, notifyUrl string) *WxPay {
	return &WxPay{
		appId:     appId,
		apiSecret: apiSecret,
		mchId:     mchId,
		tradeType: tradeType,
		notifyUrl: notifyUrl,
	}
}

func (self *WxPay) UnifyOrder(clientIp, orderId, body, fee, attach string) (map[string]string, error) {
	order := make(map[string]string)

	order["appid"] = self.appId
	order["mch_id"] = self.mchId
	order["trade_type"] = self.tradeType
	order["notify_url"] = self.notifyUrl
	order["spbill_create_ip"] = clientIp
	order["out_trade_no"] = orderId
	order["body"] = body
	order["total_fee"] = fee
	order["attach"] = attach
	order["nonce_str"] = NewNonceString()
	order["sign"] = Md5Signature(order, self.apiSecret)

	data, err := MarshalXml(order)
	if err != nil {
		return nil, err
	}

	data, err = DoHttpRequest("POST", kUnifyOrderUrl, data)
	if err != nil {
		return nil, err
	}

	resp := UnmarshalXml(data)

	if resp["sign"] != Md5Signature(resp, self.apiSecret) {
		return nil, fmt.Errorf("invalid receipt")
	}

	return resp, nil
}
